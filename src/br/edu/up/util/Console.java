package br.edu.up.util;

import java.util.Scanner;

public class Console {
	
	
	public static void imprimir(String texto) {
		System.out.println(texto); 
	}
	
	public static void separador() {
		System.out.println();
	}
	
	public static double lerDecimal(String texto) {
		imprimir(texto);
		return Double.parseDouble(lerLinha());
	}
	
	public static String lerLinha() {
		Scanner scanner = new Scanner(System.in);
		String linha = scanner.nextLine();
		scanner.close();
		return linha;
	}

}
